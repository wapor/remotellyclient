package com.bus

import com.topin.services.ClientConnection
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object RxBus {

    public val publisher = PublishSubject.create<Any>()

    fun publish(event: Any) {
        publisher.onNext(event)
    }

    // Listen should return an Observable and not the publisher
    // Using ofType we filter only events that match that class type
    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)

    fun sendMessageToServer(cmdType: String, command: String) {
        RxBus.publish(RxEvent.EventCommanderSendCommand(cmdType, hashMapOf("command" to command, "target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
    }
}