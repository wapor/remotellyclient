package com.bus

import com.topin.model.Message
import java.lang.Exception

class RxEvent {
    data class EventCommanderSendCommand(val type: String, val dataSet: HashMap<String, String>?)
    data class EventOnMessageOnMessageSender(val json: Message)

    data class EventOnConnectionSocket(val type: String)

    data class EventOnMessageFromServer(val msg: Message)
    data class EventNoTargetServerSpecifiedOnCenter(val msg: Message)
    data class EventDisconnected(val msg: String)


    data class EventOnUpdateView(val type: String, val data: HashMap<String, Any>)


    data class EventOnKeyListener(val keyCode: Int)
    data class EventOnScreenListener(val screen: Boolean)
}