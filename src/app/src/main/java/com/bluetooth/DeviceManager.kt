package com.bluetooth


import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Parcelable
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.MainActivity
import com.example.sockettestkotlin.MainActivity.Companion.activity
import com.topin.services.ClientConnection
import com.topin.services.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class DeviceManager() {
    lateinit var base : Context
    lateinit var adapter : BluetoothAdapter
    var deviceList = BluetoothDeviceList()

    constructor(base: Context) : this() {
        this.base = base
        this.adapter = BluetoothAdapter.getDefaultAdapter()


        this.registerReceivers()

        this.start()
    }

    fun start() {
        if (adapter.isEnabled) {
            adapter.startDiscovery()
        }
    }

    private fun registerReceivers() {
        if (base == null) {
            throw Exception("DeviceManager base IntentService is null!")
        }
        base.registerReceiver(deviceFoundReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
        base.registerReceiver(deviceFoundReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
        base.registerReceiver(deviceFoundReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
    }


    fun getPairedDeviceList() : BluetoothDeviceList {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        for (device in mBluetoothAdapter.bondedDevices) {
            this.deviceList.add(device.name, device.address)
        }
        return this.deviceList
    }

    fun stopReceiver() {
        this.base.unregisterReceiver(deviceFoundReceiver)
    }

    private val deviceFoundReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            val sharedPreference = context.getSharedPreferences("data-settings", Context.MODE_PRIVATE)
            if (sharedPreference != null) {
                if (! sharedPreference.getBoolean("bluetooth-enabled", false)) {
                    Log.log(this).error("disabled")
                    return
                }
            }

            if (! ClientConnection.ClientData.loggedIn) {
                Log.log(this).error("not logged in")
                return
            }

            var sensitivity: Int = 3
            var listenedDevice: String = ""
            if (sharedPreference != null) {
                sensitivity = sharedPreference.getInt("bluetooth-sensitivity", 3)
                listenedDevice = sharedPreference.getString("bluetooth-device", "").toString()
            }

            val threshold = when(sensitivity) {
                1 -> 70
                2 -> 60
                3 -> 50
                4 -> 45
                5 -> 35
                else -> 50
            }

            println("Waiting device: $listenedDevice")
            println("Waiting threshold RSSI: $threshold")


            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED == action) {
                println("Discovery Started")
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                println("Discovery finished")
                adapter.startDiscovery()
            } else if (BluetoothDevice.ACTION_FOUND == action) {
                val device =
                    intent.getParcelableExtra<Parcelable>(BluetoothDevice.EXTRA_DEVICE)
                            as BluetoothDevice

                val rssi =
                    intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, java.lang.Short.MIN_VALUE)
                println("Found: ${device.name} - $rssi")
                deviceList.updateRssi(device.address, rssi.toInt())

                if (device.address != listenedDevice) {
                    return
                }
                println("Check RSSI: $rssi on ${device.name}")


                if (rssi < (threshold*-1)) {
                    RxBus.publish(
                        RxEvent.EventCommanderSendCommand(
                            "lock",
                            hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())
                        )
                    )
                }

               /* if (rssi < -45) {
                    RxBus.publish(
                        RxEvent.EventCommanderSendCommand(
                            "lock",
                            hashMapOf("target" to "12345", "from" to "0123")
                        )
                    )
                }*/
                //println(rssi.toString())
            }
        }
    }

}