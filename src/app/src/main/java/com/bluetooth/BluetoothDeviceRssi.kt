package com.bluetooth

class BluetoothDeviceRssi(var name: String, var mac: String, var rssi: Int? = null) {
    override fun toString(): String {
        return "BluetoothDeviceRssi(name='$name', mac='$mac', rssi=$rssi)"
    }
}