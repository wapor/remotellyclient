package com.bluetooth

class BluetoothDeviceList {
    var deviceList : ArrayList<BluetoothDeviceRssi> = ArrayList()

    fun add(name: String, mac: String): BluetoothDeviceList {
        var device = BluetoothDeviceRssi(name, mac)
        deviceList.add(device)
        return this
    }

    fun updateRssi(mac: String, rssi: Int) {
        val device = (this.deviceList.find { it.mac == mac })
        if (device != null) {
            device.rssi = rssi
        }
    }

    fun getByName(name: String): BluetoothDeviceRssi? {
        return (this.deviceList.find { it.name == name })
    }

    fun getByMac(mac: String): BluetoothDeviceRssi? {
        return (this.deviceList.find { it.mac == mac })
    }

    override fun toString(): String {
        return "BluetoothDeviceList(deviceList=$deviceList)"
    }
}