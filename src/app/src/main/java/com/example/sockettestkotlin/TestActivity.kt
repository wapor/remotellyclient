package com.example.sockettestkotlin

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class TestActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var rotationVector: Sensor
    private lateinit var linearAcceleration: Sensor
    private lateinit var gyroscope: Sensor
    private lateinit var gravity: Sensor
    private lateinit var accelerometer: Sensor
    private lateinit var sensorManager: SensorManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        this.sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)?.let {
            this.accelerometer = it
        }

        sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY)?.let {
            this.gravity = it
        }

        sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)?.let {
            this.gyroscope = it
        }

        sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)?.let {
            this.linearAcceleration = it
        }

        sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)?.let {
            this.rotationVector = it
        }

        println(this.accelerometer)
        println(this.gravity)
        println(this.gyroscope)
        println(this.linearAcceleration)
        println(this.rotationVector)

    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //println(accelerometer)
    }

    private var lastUpdate: Long = 0
    private var last_x = 0f
    private  var last_y:kotlin.Float = 0f
    private  var last_z:kotlin.Float = 0f
    private val SHAKE_THRESHOLD = 600

    override fun onSensorChanged(sensorEvent: SensorEvent?) {
        /*val millibarsOfPressure = event!!.values[0]
        if (event.sensor.type == Sensor.TYPE_LIGHT)
            Toast.makeText(this, "" + millibarsOfPressure + " lx", Toast.LENGTH_SHORT).show()*/

        val mySensor: Sensor = sensorEvent!!.sensor

        if (mySensor.type == Sensor.TYPE_ACCELEROMETER) {
            val x: Float = sensorEvent!!.values[0]
            val y: Float = sensorEvent.values[1]
            val z: Float = sensorEvent.values[2]
            val curTime = System.currentTimeMillis()
            if (curTime - lastUpdate > 100) {
                val diffTime: Long = curTime - lastUpdate
                lastUpdate = curTime
                val speed: Float =
                    Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000
                if (speed > SHAKE_THRESHOLD) {
                }
                last_x = x
                last_y = y
                last_z = z
            }

            println("$x $y $z")
        }
    }


}