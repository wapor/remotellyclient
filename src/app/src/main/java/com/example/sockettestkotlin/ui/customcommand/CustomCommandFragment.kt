package com.example.sockettestkotlin.ui.customcommand

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bus.RxBus
import com.example.sockettestkotlin.DialogManager
import com.example.sockettestkotlin.R


class CustomCommandFragment : Fragment() {

    private lateinit var customCommandViewModel: CustomCommandViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        customCommandViewModel =
            ViewModelProviders.of(this).get(CustomCommandViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_custom_command, container, false)
        //val textView: TextView = root.findViewById(R.id.text_send)
        //customCommandViewModel.text.observe(this, Observer {
         //   textView.text = it
        //})

        initListeners(root)

        return root
    }

    private fun initListeners(root: View) {
        root.findViewById<Button>(R.id.button_sendMessage).setOnClickListener {
            val msgCommand = root.findViewById<EditText>(R.id.editText_sendMessage).text.toString()
            if (msgCommand.isEmpty()) {
                DialogManager(root.context, "error", "The command cannot be empty", "Please specify the command.").show()
            } else {
                DialogManager(root.context, "success", "The message has been sent", "").show()
                RxBus.sendMessageToServer("customCommand", msgCommand)
            }
        }
    }

}