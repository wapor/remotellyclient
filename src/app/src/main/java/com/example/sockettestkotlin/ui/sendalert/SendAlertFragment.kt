package com.example.sockettestkotlin.ui.sendalert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bus.RxBus
import com.example.sockettestkotlin.DialogManager
import com.example.sockettestkotlin.R

class SendAlertFragment : Fragment() {

    private lateinit var sendAlertViewModel: SendAlertViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_send_alert, container, false)

        initListeners(root)

        return root
    }

    private fun initListeners(root: View) {
        root.findViewById<Button>(R.id.button_sendAlertMessage).setOnClickListener {
            val msgCommand = root.findViewById<EditText>(R.id.editText_sendAlertMessage).text.toString()
            if (msgCommand.isEmpty()) {
                DialogManager(root.context, "error", "The alert message cannot be empty", "Please specify the alert message.").show()
            } else {
                DialogManager(root.context, "success", "The alert message has been sent", "").show()
                RxBus.sendMessageToServer("alert", msgCommand)
            }
        }
    }
}