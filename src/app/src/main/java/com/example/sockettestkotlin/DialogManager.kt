package com.example.sockettestkotlin

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import android.widget.*
import java.lang.Exception

class DialogManager(
    private var context: Context,
    private var type: String,
    private var title: String,
    private var description: String
) {
    var dialog: Dialog = Dialog(this.context)
    private lateinit var titleTextView: TextView
    private lateinit var msgTextView: TextView
    private lateinit var image: ImageView
    private lateinit var okButton: Button
    private var cancelButton: Button? = null
    private lateinit var closeButton: ImageButton
    private var layoutId: Int = -1

    init {
        this.layoutId = when(this.type) {
            "info" -> R.layout.dialog_single
            "confirm" -> R.layout.dialog_double
            "success" -> R.layout.dialog_single
            "error" -> R.layout.dialog_single
            "warning" -> R.layout.dialog_single
            "progress" -> R.layout.dialog_progress
            else -> throw Exception("Undefined dialog type")
        }
        val imgResource= when(this.type) {
            "info" -> R.drawable.dialog_image_info
            "success" -> R.drawable.dialog_image_success
            "error" -> R.drawable.dialog_image_error
            "warning" -> R.drawable.dialog_image_warning
            else -> -1
        }
        initDialogMain()

        initTitle()
        initDescription()

        if (this.type != "progress") {
            initDialogImage(imgResource)

            initOkButton()
            initCancelButton()

            initCloseButton()

            okButton.setOnClickListener { dialog.cancel() }
            cancelButton?.setOnClickListener { dialog.cancel() }
            closeButton.setOnClickListener { dialog.cancel() }
        }
    }

    private fun getLayoutParams(dialog: Dialog): WindowManager.LayoutParams {
        val layoutParams = WindowManager.LayoutParams()
        if (dialog.window != null) {
            layoutParams.copyFrom(dialog.window?.attributes)
        }
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT

        return layoutParams
    }

    private fun initDialogMain() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(layoutId)
        dialog.setCanceledOnTouchOutside(false)
    }

    private fun initOkButton() {
        okButton = dialog.findViewById(R.id.okButton)
        okButton.text = "OK"
    }

    private fun initCancelButton() {
        if (this.layoutId == R.layout.dialog_double) {
            cancelButton = dialog.findViewById(R.id.cancelButton)
            cancelButton?.text = "Cancel"
        }
    }

    private fun initCloseButton() {

        closeButton = dialog.findViewById(R.id.closeButton)
        if (dialog.window != null) {
            dialog.window?.attributes = getLayoutParams(dialog)
        }
    }

    private fun initDialogImage(imgResource: Int) {
        image = dialog.findViewById(R.id.dialogImageView)
        image.setImageResource(imgResource)
    }

    private fun initTitle() {
        titleTextView = dialog.findViewById(R.id.titleTextView)
        titleTextView.text = this.title
    }

    private fun initDescription() {
        msgTextView = dialog.findViewById(R.id.messageTextView)
        msgTextView.text = this.description
    }

    fun show() {
        dialog.show()
    }

    fun close() {
        dialog.cancel()
    }

    fun reopen() {
        if (! this.dialog.isShowing) {
            this.show()
        }
    }


    /*loadingDialog.dialog.setOnKeyListener { v, keyCode, event ->
            println(v)
            println(keyCode)
            println(event)
            false
        }
        loadingDialog.show()*/
}