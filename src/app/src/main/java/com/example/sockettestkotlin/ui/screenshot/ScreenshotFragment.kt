package com.example.sockettestkotlin.ui.screenshot

import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.DialogManager
import com.example.sockettestkotlin.R
import com.github.chrisbanes.photoview.PhotoView
import com.topin.services.ClientConnection
import io.reactivex.disposables.Disposable


class ScreenshotFragment : Fragment() {

    private lateinit var toolsViewModel: ScreenshotViewModel
    private var loadingDialog: DialogManager? = null
    private lateinit var updateViewListener: Disposable

    private val imageScale = 0f

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        toolsViewModel =
            ViewModelProviders.of(this).get(ScreenshotViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_screenshot, container, false)
        this.loadingDialog = DialogManager(root?.context!!, "progress", "Loading screenshot...", "It maybe takes long time...")
        loadingDialog?.dialog?.setCancelable(true)
        loadingDialog?.dialog?.setOnKeyListener { dialogInterface: DialogInterface, i: Int, keyEvent: KeyEvent ->
            fragmentManager!!.popBackStack()
            i == KeyEvent.KEYCODE_BACK
        }
        //val textView: TextView = root.findViewById(R.id.text_tools)
        //toolsViewModel.text.observe(this, Observer {
        //    textView.text = it
        //})

        listening()

        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "request",
                hashMapOf(
                    "request" to "screenshot",
                    "parameter" to "",
                    "from" to ClientConnection.ClientData.loginToken.toString(),
                    "target" to "1"
                )
            )
        )

        run {
            updateViewListener = RxBus.listen(RxEvent.EventOnUpdateView::class.java).subscribe( {
                when (it.type) {
                    "screenshotFragment" -> {
                        println(it)
                        activity?.runOnUiThread {
                            val scr = root.findViewById<PhotoView>(R.id.image_screenshot)
                            val strBase64 = it.data["imageBase64"].toString()


                            val decodedString: ByteArray = android.util.Base64.decode(strBase64, android.util.Base64.DEFAULT)
                            val decodedByte =
                                BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

                            scr.setImageBitmap(decodedByte)

                            if (this.loadingDialog?.dialog!!.isShowing) {
                                this.loadingDialog?.close()
                            }
                        }
                    }
                }
            }, {
                it.printStackTrace()
            })
        }




        return root
    }

    private fun listening() {
        this.loadingDialog?.show()
    }

    override fun onResume() {
        super.onResume()
        listening()
    }


    override fun onDestroy() {
        super.onDestroy()

        updateViewListener.dispose()

        if (this.loadingDialog != null) {
            this.loadingDialog?.close()
        }
    }
}