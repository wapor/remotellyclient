package com.example.sockettestkotlin

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}