package com.example.sockettestkotlin.ui.settings


import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.sockettestkotlin.R
import com.topin.managers.BluetoothService
import kotlinx.android.synthetic.main.fragment_settings.view.*


class SettingsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        val sharedPreference = activity?.getSharedPreferences("data-settings", Context.MODE_PRIVATE)
        val editor = sharedPreference!!.edit()
        //var selectedDeviceIndex: Int? = null

        val allPairedDeviceList = getPairedDeviceList()
        val selectedDeviceAddress = sharedPreference.getString("bluetooth-device", null)
        val selectedDevice = allPairedDeviceList.find { it.address == selectedDeviceAddress }
        var selectedDeviceName: String? = null
        if (selectedDevice != null) {
            selectedDeviceName = selectedDevice.name
        }
        println(selectedDevice?.name)

        /*if (selectedDeviceAddress != null) {
            if (allPairedDeviceList.count() > 0) {
                selectedDeviceIndex = allPairedDeviceList.indexOfFirst { it.address == selectedDeviceAddress }
            }
        }*/


        root.seekBar_settings_bluetooth_sensitivity.progress = sharedPreference.getInt("bluetooth-sensitivity", 0)
        root.switch_settings_bluetooth.isChecked = sharedPreference.getBoolean("bluetooth-enabled", false)
        changeBluetoothSettings(root, sharedPreference.getBoolean("bluetooth-enabled", false))

        val deviceList = this.getPairedDeviceList().map {
            it.name
        }
        val adapter: SpinnerAdapter = ArrayAdapter<String>(
            root.context,
            R.layout.support_simple_spinner_dropdown_item,
            deviceList
        )

        root.spinner_settings_bluetooth_device.adapter = adapter

        setupListeners(root, editor)


      /*  if (selectedDeviceName != null) {
            val spinnerPosition: Int = adapter.get(selectedDeviceName)
            root.spinner_settings_bluetooth_device.setSelection(spinnerPosition)
        }*/

        if (selectedDeviceName != null) {
            selectSpinnerValue(root.spinner_settings_bluetooth_device, selectedDeviceName)
        }

        return root
    }

    private fun setupListeners(
        root: View,
        editor: SharedPreferences.Editor
    ) {
        root.switch_settings_bluetooth.setOnCheckedChangeListener { buttonView, isChecked ->
            editor.putBoolean("bluetooth-enabled", isChecked)
            editor.apply()

            if (isChecked) {
                BluetoothService.startService(root.context, "Bluetooth service is running...")
            } else {
                BluetoothService.stopService(root.context)
            }

            changeBluetoothSettings(root, isChecked)
        }

        root.seekBar_settings_bluetooth_sensitivity.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                editor.putInt("bluetooth-sensitivity", i)
                editor.apply()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        root.spinner_settings_bluetooth_device.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val device = getPairedDeviceList()[position]
                editor.putString("bluetooth-device", device.address)
                editor.apply()
            }

        }
    }

    private fun changeBluetoothSettings(root: View, isChecked: Boolean) {
        if (isChecked) {
            root.seekBar_settings_bluetooth_sensitivity.visibility = View.VISIBLE
            root.spinner_settings_bluetooth_device.visibility = View.VISIBLE
            root.textView_sensitivity.visibility = View.VISIBLE
            root.textView_selected_device.visibility = View.VISIBLE
        } else {
            root.seekBar_settings_bluetooth_sensitivity.visibility = View.GONE
            root.spinner_settings_bluetooth_device.visibility = View.GONE
            root.textView_sensitivity.visibility = View.GONE
            root.textView_selected_device.visibility = View.GONE
        }
    }

    private fun getPairedDeviceList() : ArrayList<BluetoothDevice> {
        var deviceList = ArrayList<BluetoothDevice>()

        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        for (device in mBluetoothAdapter.bondedDevices) {
            deviceList.add(device)
        }
        return deviceList
    }

    private fun selectSpinnerValue(spinner: Spinner, myString: String) {
        val index = 0
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString() == myString) {
                spinner.setSelection(i)
                break
            }
        }
    }
}