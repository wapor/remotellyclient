package com.example.sockettestkotlin

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.volume.SettingsContentObserver
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.topin.managers.BluetoothService
import com.topin.services.ClientConnection
import com.topin.services.Log
import io.reactivex.disposables.Disposable


class BaseActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var disposable: Disposable
    private lateinit var disposable2: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (! ClientConnection.ClientData.connectedToServer) {
            //finish() /// TODO: MEGCSINÁLNI HOGYHA ÚJRAINDUL A SZERVER, AKKOR PRÓBÁLJON MEG ÚJRACSATLAKOZNI
            //return
        }

        val sharedPreference = getSharedPreferences("data-settings", Context.MODE_PRIVATE)
        val bluetoothListenerIsEnabled = sharedPreference.getBoolean("bluetooth-enabled", false)
        if (! BluetoothService.isActive && bluetoothListenerIsEnabled) {
            BluetoothService.startService(this, "")
        }
        if (! bluetoothListenerIsEnabled) {
            BluetoothService.stopService(this)
        }


        val mSettingsContentObserver = SettingsContentObserver(this, Handler())
        applicationContext.contentResolver.registerContentObserver(
            Settings.System.CONTENT_URI,
            true,
            mSettingsContentObserver
        )

        setContentView(R.layout.activity_base)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "You can lock your server. Send Lock Request:", Snackbar.LENGTH_LONG)
                .setAction("LOCK IT!") {
                    RxBus.publish(RxEvent.EventCommanderSendCommand("lock", hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
                    val succcessDialog = DialogManager(this, "success", "Lock has been sent!", "").show()
                }.show()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_send_command, R.id.nav_send_alert, R.id.nav_task_list,
                R.id.nav_screenshot, R.id.nav_custom_command, R.id.nav_settings,
                R.id.nav_logout, R.id.nav_screen_live, R.id.nav_mouse_move, R.id.nav_presentation
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        this.disposable = RxBus.listen(RxEvent.EventOnConnectionSocket::class.java).take(1).subscribe({
            runOnUiThread {
                when (it.type) {
                    "connectionError" -> {
                        ClientConnection.ClientData.connectedToServer = false
                        val intent = Intent(this, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    }
                }
            }
        }, {
            Log.log(this).error(it.message)
        })


        /*val intentFilter = IntentFilter(Intent.ACTION_SCREEN_ON)
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
        registerReceiver(object: BroadcastReceiver() {
            override fun onReceive(context: Context, intent:Intent) {
                if (intent.action == Intent.ACTION_SCREEN_OFF) {
                    ClientConnection.ClientData.screenOn = false
                }
                else if (intent.action == Intent.ACTION_SCREEN_ON) {
                    ClientConnection.ClientData.screenOn = true
                }

                RxBus.publish(RxEvent.EventOnScreenListener(ClientConnection.ClientData.screenOn))
            }
        }, intentFilter)*/

        listenTargetServerIsActive()
        listenLogout()
    }

    private fun listenLogout() {
        //findViewById<MenuItem>(R.id.nav_logout)
    }

    private fun logout() {
        val sharedPreferences = getSharedPreferences("data-login", Context.MODE_PRIVATE)
        sharedPreferences.edit().remove("username").remove("password").apply()

        val bluetoothListenerIsEnabled = getSharedPreferences("data-settings", Context.MODE_PRIVATE).getBoolean("bluetooth-enabled", false)
        if (! bluetoothListenerIsEnabled) {
            BluetoothService.stopService(this)
        }

        ClientConnection.ClientData.loginToken = null
        ClientConnection.ClientData.connectedToServer = false

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask()
        } else {
            this.finishAffinity()
        }
    }

    private fun listenTargetServerIsActive() {
        run {
            RxBus.listen(RxEvent.EventNoTargetServerSpecifiedOnCenter::class.java).take(1).subscribe({

                val errorActivity = Intent(this, ErrorActivity::class.java)
                errorActivity.putExtra("title", "Server connection error...")
                errorActivity.putExtra("description", "The remote server (PC) has not connected to center. Please try again later, or please connect the server to the center...")
                errorActivity.putExtra("button_ok", "Try again")
                errorActivity.putExtra("button_cancel", "Exit application")

                startActivity(errorActivity)

                finish()

                ClientConnection.ClientData.targetInitData = null
            }, {})
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.base, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode != KeyEvent.KEYCODE_VOLUME_UP && keyCode != KeyEvent.KEYCODE_VOLUME_DOWN) {
            RxBus.publish(RxEvent.EventOnKeyListener(keyCode))
        }

        when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_UP -> {
                if (ClientConnection.ClientData.onPresentation) {
                    return false
                }
            }
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                if (ClientConnection.ClientData.onPresentation) {
                    return false
                }
            }
            else -> return false
        }
        return super.onKeyDown(keyCode, event)
    }
}