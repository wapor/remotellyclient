package com.example.sockettestkotlin

import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sockettestkotlin.adapters.ActionListAdapter
import com.example.sockettestkotlin.objects.ActionObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*

class ActionListActivity : AppCompatActivity() {
    internal lateinit var adapter: ActionListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        initUI()

        adapter.setOnItemClickListener(object : ActionListAdapter.OnItemClickListener{
            override fun onItemClick(view: View, obj: ActionObject, position: Int) {
                Toast.makeText(applicationContext, "Clicked " + obj.title, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            Toast.makeText(applicationContext, item.title, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(applicationContext, item.title, Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        initToolbar()

        val list = listOf(
            ActionObject("Shutdown", "icon_shutdown", "shutdown"),
            ActionObject("Restart", "icon_restart", "restart"),
            ActionObject("Lock", "icon_lock", "lock")
        )

        // get list adapter
        //adapter = ActionListAdapter(courseList)
        adapter = ActionListAdapter(list)

        val mLayoutManager = GridLayoutManager(applicationContext, 3)
        this.videoRecyclerView.layoutManager = mLayoutManager
        this.videoRecyclerView.itemAnimator = DefaultItemAnimator()
        this.videoRecyclerView.adapter = adapter
    }


    private fun initToolbar() {
        try {
            toolbar.setNavigationIcon(R.drawable.baseline_menu_black_24)
            toolbar.title = "Select action"
            if (toolbar.navigationIcon != null) {
                toolbar.navigationIcon?.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP)
            }
            setSupportActionBar(toolbar)
            if (supportActionBar != null) {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        } catch (e: Exception) {
            Log.e("TOOLBAR", "Toolbar error: ${e.message}")
        }

    }

}
