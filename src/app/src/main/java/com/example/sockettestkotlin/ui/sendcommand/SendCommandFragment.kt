package com.example.sockettestkotlin.ui.sendcommand

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.R
import com.example.sockettestkotlin.adapters.ActionListAdapter
import com.example.sockettestkotlin.objects.ActionObject
import com.topin.services.ClientConnection

class SendCommandFragment : Fragment() {

    private lateinit var sendCommandViewModel: SendCommandViewModel
    internal lateinit var adapter: ActionListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sendCommandViewModel =
            ViewModelProviders.of(this).get(SendCommandViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_send_command, container, false)

        initUI(root)
        initListener()

        return root
    }

    private fun initListener() {
        adapter.setOnItemClickListener(object : ActionListAdapter.OnItemClickListener{
            override fun onItemClick(view: View, obj: ActionObject, position: Int) {
                when (obj.action) {
                    "shutdown" -> RxBus.publish(RxEvent.EventCommanderSendCommand("shutdown", hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
                    "shutdownCancel" -> RxBus.publish(RxEvent.EventCommanderSendCommand("shutdownCancel", hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
                    "restart" -> RxBus.publish(RxEvent.EventCommanderSendCommand("restart", hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
                    "lock" -> RxBus.publish(RxEvent.EventCommanderSendCommand("lock", hashMapOf("target" to "1", "from" to ClientConnection.ClientData.loginToken.toString())))
                    "playSoundNotify" -> RxBus.sendMessageToServer("playSound", "c:\\WINDOWS\\Media\\notify.wav")
                    "openBrowser" -> RxBus.sendMessageToServer("openBrowser", "")
                    "startProcess_Explorer" -> RxBus.sendMessageToServer("startProcess", "explorer")
                    "startProcess_Calc" -> RxBus.sendMessageToServer("startProcess", "calc")
                    "startProcess_Diskmgmt" -> RxBus.sendMessageToServer("startProcess", "diskmgmt")
                    "starwars" -> RxBus.sendMessageToServer("customCommand", "start cmd /k Telnet Towel.blinkenlights.nl")
                    else -> RxBus.sendMessageToServer(obj.action, "")
                }
                Toast.makeText(activity, "Send " + obj.title, Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun initUI(root: View) {
        val list = listOf(
            ActionObject("Shutdown", "ic_pause_circle_outline_black_54dp", "shutdown"),
            ActionObject("Restart", "ic_settings_backup_restore_black_54dp", "restart"),
            ActionObject("Lock", "ic_lock_outline_black_54dp", "lock"),
            ActionObject("Shutdown Cancel", "ic_pan_tool_black_54dp", "shutdownCancel"),
            ActionObject("Open Browser", "ic_open_in_browser_black_54dp", "openBrowser"),
            ActionObject("Volume Mute", "icon_volume_mute_grey_54dp", "volumeControlMute"),
            ActionObject("Volume Down", "icon_volume_down_grey_54dp", "volumeControlDown"),
            ActionObject("Volume Up", "icon_volume_up_grey_54dp", "volumeControlUp"),
            ActionObject("Play Notify Sound", "ic_notifications_active_black_54dp", "playSoundNotify"),
            ActionObject("File Explorer", "ic_format_align_justify_black_54dp", "startProcess_Explorer"),
            ActionObject("Calculator", "ic_filter_9_plus_black_54dp", "startProcess_Calc"),
            ActionObject("Disk Management", "ic_attach_file_black_54dp", "startProcess_Diskmgmt"),
            ActionObject("Easter Egg", "ic_adb_black_54dp", "starwars")
        )

        // get list adapter
        //adapter = ActionListAdapter(courseList)
        adapter = ActionListAdapter(list)

        val listView = root.findViewById(R.id.recyclerView) as RecyclerView

        val mLayoutManager = GridLayoutManager(activity, 3)
        listView.layoutManager = mLayoutManager
        listView.itemAnimator = DefaultItemAnimator()
        listView.adapter = adapter
    }
}