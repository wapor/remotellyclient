package com.example.sockettestkotlin.ui.errors

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sockettestkotlin.R
import kotlinx.android.synthetic.main.fragment_error_data.view.*

class ErrorFragment : Fragment() {

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_error_data, container, false)

        //root.textView_error_message_title.text = title
        //root.textView_error_message_details.text = description

        return root
    }

    fun setup(root: View, title: String, description: String) {
        root.textView_error_message_title.text = title
        root.textView_error_message_details.text = description
    }

}