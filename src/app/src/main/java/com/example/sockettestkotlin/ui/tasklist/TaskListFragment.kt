package com.example.sockettestkotlin.ui.tasklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bus.RxBus
import com.example.sockettestkotlin.R
import com.example.sockettestkotlin.adapters.TaskListAdapter
import com.example.sockettestkotlin.objects.SingleTaskObject
import com.topin.services.ClientConnection


class TaskListFragment : Fragment() {

    internal lateinit var adapter: TaskListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_task_list, container, false)


        initUI(root)
        initListener()

        return root
    }

    private fun initListener() {
        adapter.setOnItemClickListener(object : TaskListAdapter.OnItemClickListener{
            override fun onItemClick(view: View, obj: SingleTaskObject, position: Int) {

                Toast.makeText(activity, " " + obj.name, Toast.LENGTH_SHORT).show()
                RxBus.sendMessageToServer("taskKill", obj.name)
            }
        })
    }


    private fun initUI(root: View) {
        val list = arrayListOf<SingleTaskObject>()
        val taskListFromOriginal = ClientConnection.ClientData.targetInitData?.getTaskListAsArray()
        var last = ""
        taskListFromOriginal?.sortBy {
            it.name.toLowerCase()
        }
        taskListFromOriginal?.forEach {
            if(it.name != last) {
                it.header = true
            }
            last = it.name
        }
        val taskListArray = taskListFromOriginal


        if (taskListArray != null) {
            for(singleTask in taskListArray) {
                if(singleTask.header) {
                    list.add(
                        SingleTaskObject(
                            singleTask.pid,
                            singleTask.name,
                            singleTask.header,
                            singleTask.memUsage
                        )
                    )
                }
            }
        }

        // get list adapter
        //adapter = ActionListAdapter(courseList)
        adapter = TaskListAdapter(list)

        val listView = root.findViewById(R.id.recyclerView_taskList) as RecyclerView

        val mLayoutManager = GridLayoutManager(activity, 1)
        listView.layoutManager = mLayoutManager
        listView.itemAnimator = DefaultItemAnimator()
        listView.adapter = adapter
    }
}