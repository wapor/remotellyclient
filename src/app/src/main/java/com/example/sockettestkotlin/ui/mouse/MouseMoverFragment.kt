package com.example.sockettestkotlin.ui.mouse

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.LoginActivity
import com.example.sockettestkotlin.R
import com.topin.services.ClientConnection
import com.topin.services.Log
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_mouse_mover.view.*


class MouseMoverFragment : Fragment(), SensorEventListener {
    var sm: SensorManager? = null
    var sensitivity = 40

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_mouse_mover, container, false)

        // SENSOR MANAGER
        sm = activity!!.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        root.button_left_click.setOnTouchListener { view: View, motionEvent: MotionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_DOWN){
                sendClick("left", 1)
            }
            else if(motionEvent.action == MotionEvent.ACTION_UP){
                sendClick("left", 2)
            }
            true
        }

        root.button_right_click.setOnTouchListener { view: View, motionEvent: MotionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_DOWN){
                sendClick("right", 1)
            }
            else if(motionEvent.action == MotionEvent.ACTION_UP){
                sendClick("right", 2)
            }
            true
        }

        root.eventKey_left.setOnClickListener { sendButton(0x25) }
        root.eventKey_right.setOnClickListener { sendButton(0x27) }
        root.eventKey_down.setOnClickListener { sendButton(0x28) }
        root.eventKey_up.setOnClickListener { sendButton(0x26) }
        root.eventKey_esc.setOnClickListener { sendButton(0x1B) }


        return root
    }

    fun sendClick(button: String, clickType: Int) {
        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "mouseClick",
                hashMapOf(
                    "button" to button,
                    "mouseType" to clickType.toString(),
                    "from" to ClientConnection.ClientData.loginToken.toString(),
                    "target" to "1"
                )
            )
        )
    }

    fun sendButton(keyCode: Int) {
        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "keyCode",
                hashMapOf(
                    "keyCode" to keyCode.toString(),
                    "from" to ClientConnection.ClientData.loginToken.toString(),
                    "target" to "1"
                )
            )
        )
    }

    override fun onResume() {
        super.onResume()

        val typedSensors = sm!!.getSensorList(Sensor.TYPE_GYROSCOPE)
        if (typedSensors == null || typedSensors.size <= 0) {
        } else {
            sm!!.registerListener(
                this, typedSensors[0],
                SensorManager.SENSOR_DELAY_GAME
            )
        }
    }

    override fun onAccuracyChanged(arg0: Sensor, arg1: Int) { }

    override fun onSensorChanged(event: SensorEvent) { // SEND A MOUSE POSITION TO THE PC

        val x = (event.values[2] * sensitivity).toInt()
        val y = (event.values[0] * sensitivity).toInt()

        if (x > 1 || x < -1 || y > 1 || y < -1) {
        //if (x != 0 || y != 0) {
            println("$x | $y")
            RxBus.publish(
                RxEvent.EventCommanderSendCommand(
                    "mouseMove",
                    hashMapOf(
                        "x" to x.toString(),
                        "y" to y.toString(),
                        "from" to ClientConnection.ClientData.loginToken.toString(),
                        "target" to "1"
                    )
                )
            )
        }
    }

    override fun onPause() {
        sm!!.unregisterListener(this)
        super.onPause()
    }

    override fun onStop() {
        sm!!.unregisterListener(this)
        super.onStop()
    }



}