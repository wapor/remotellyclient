package com.example.sockettestkotlin.ui.home

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.DialogManager
import com.example.sockettestkotlin.R
import com.topin.model.command.InitMessage
import com.topin.services.ClientConnection
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    private var loadingDialog: DialogManager? = null
    private lateinit var updateViewListener: Disposable

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        this.loadingDialog = DialogManager(root?.context!!, "progress", "Loading system information...", "It maybe takes long time...")
        loadingDialog?.dialog?.setCancelable(false)

        if (! ClientConnection.ClientData.connectedToServer) {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            return root
        }

        listening()

        run {
            updateViewListener = RxBus.listen(RxEvent.EventOnUpdateView::class.java).subscribe( {
                when (it.type) {
                    "homeFragment" -> {
                        println(it)
                        activity?.runOnUiThread {
                            loadingDialog!!.close()
                            loadData(root, ClientConnection.ClientData.targetInitData)
                        }
                    }
                }
            }, {
                it.printStackTrace()
            })
        }

        val initMessage = ClientConnection.ClientData.targetInitData
        this.loadData(root, initMessage)

        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "request",
                hashMapOf(
                    "request" to "init",
                    "parameter" to "",
                    "from" to ClientConnection.ClientData.loginToken.toString(),
                    "target" to "1"
                )
            )
        )



        root.imageView_refresh_home_profile.setOnClickListener {
            RxBus.publish(
                RxEvent.EventCommanderSendCommand(
                    "request",
                    hashMapOf(
                        "request" to "init",
                        "parameter" to "",
                        "from" to ClientConnection.ClientData.loginToken.toString(),
                        "target" to "1"
                    )
                )
            )
            this.loadingDialog?.show()
            GlobalScope.launch {
                delay(3000)
                activity?.runOnUiThread {
                    loadingDialog?.close()
                    loadData(root, ClientConnection.ClientData.targetInitData)
                }
            }
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        listening()
    }

    private fun listening() {
        if (ClientConnection.ClientData.targetInitData == null) {
            loadingDialog!!.show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun loadData(root: View, initMessage: InitMessage?) {
        if (initMessage == null) {
            //waitForInitData(root)
        } else {
            root.textView_profile_hostname.text = initMessage.hostname
            root.textView_profile_ipv4.text = initMessage.localIp

            root.textView_profile_details_hostname.text = initMessage.hostname

            root.textView_profile_ramPercentage.text = initMessage.getRamPercentage().toString() + "%"
            root.textView_profile_ramFull.text = initMessage.getRamUsageText()

            root.textView_profile_cpuUsage.text = initMessage.cpuUsage.toString() + "%"

            root.textView_profile_details_cpuName.text = initMessage.cpuName
            root.textView_profile_details_osName.text = initMessage.osName
            root.textView_profile_details_osVersion.text = initMessage.osVersion
            root.textView_profile_details_biosVersion.text = initMessage.biosVersion
            root.textView_profile_details_localIp.text = initMessage.localIp

            val scr = root.findViewById<ImageView>(R.id.imageView_profile_background)

            val decodedString: ByteArray = android.util.Base64.decode(initMessage.backgroundImage, android.util.Base64.DEFAULT)
            val decodedByte =
                BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            scr.setImageBitmap(decodedByte)
        }
    }

  /*  private fun waitForInitData(root: View?) {
        val loadingDialog = DialogManager(root?.context!!, "progress", "Loading system information...", "It maybe takes long time...")
        loadingDialog.dialog.setCancelable(false)

        loadingDialog.show()


        // childFragmentManager.beginTransaction().replace(R.id.nav_host_fragment,SendAlertFragment()).commit()


    }*/

    private fun waitForInitData(root: View?) {

        loadingDialog!!.show()

        if (root != null) {
            if(root.isVisible) {
                if (this.loadingDialog != null) {
                    this.loadingDialog?.close()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        updateViewListener.dispose()

        if (this.loadingDialog != null) {
            this.loadingDialog?.close()
        }
    }

    override fun onPause() {
        super.onPause()

        if (this.loadingDialog != null) {
            this.loadingDialog?.close()
        }
    }

}