package com.example.sockettestkotlin.ui.customcommand

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CustomCommandViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Custom Command fragment"
    }
    val text: LiveData<String> = _text
}