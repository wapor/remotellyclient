package com.example.sockettestkotlin

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.bus.RxBus
import com.bus.RxEvent
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import android.os.Bundle
import com.example.designtest.Utils
import com.topin.services.Log


class MainActivity : AppCompatActivity() {

    private lateinit var disposable: Disposable

    companion object {
        var activity: Activity? = null
    }

    fun clickButtonSendAlert(view: View) {
        val alertMessage = editText1.text.toString()
        editText1.text.clear()

        RxBus.publish(RxEvent.EventCommanderSendCommand("alert", hashMapOf("message" to alertMessage)))
    }

    fun clickButtonSendLock(view: View) = RxBus.publish(RxEvent.EventCommanderSendCommand("lock", hashMapOf("target" to "12345", "from" to "0123")))
    fun clickButtonSendShutdown(view: View) = RxBus.publish(RxEvent.EventCommanderSendCommand("shutdown", null))
    fun clickButtonLogin(view: View) = RxBus.publish(RxEvent.EventCommanderSendCommand("login", hashMapOf("token" to "0111")))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity = this@MainActivity

        Utils.requestPremissions(this)


        this.disposable = RxBus.listen(RxEvent.EventCommanderSendCommand::class.java).subscribe({
            runOnUiThread {
                if (it.dataSet != null) {
                    Toast.makeText(
                        applicationContext,
                        "Alert Message sent: " + it.dataSet["message"],
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Command sent: " + it.type,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }, {
            Log.log(this).error(it.message)
        })

        // CHECK BLUETOOTH DEVICE, AND SEND IF SHOULD SEND THE [LOCK] COMMAND
        //DeviceManager(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
        //stopService(this.bluetoothServiceIntent)
    }


}