package com.example.sockettestkotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bus.RxBus
import com.bus.RxEvent
import com.topin.services.ClientConnection
import kotlinx.android.synthetic.main.activity_error_data.*


class ErrorActivityNoConnection : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error_data)

        init()
        initListeners()
    }

    private fun initListeners() {
        this.button_cancel.setOnClickListener {
            finish()
        }
    }

    private fun init() {
        this.textView_error_message_title_activity.text = intent.getStringExtra("title")
        this.textView_error_message_details_activity.text = intent.getStringExtra("description")
        this.button_ok.visibility = View.GONE
        this.button_cancel.text = intent.getStringExtra("button_cancel")
    }


}
