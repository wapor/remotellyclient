package com.example.sockettestkotlin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bus.RxBus
import com.bus.RxEvent
import com.topin.model.command.StatusMessage
import com.topin.services.ClientConnection
import com.topin.services.Log
import com.topin.services.Toaster
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class LoginActivity : AppCompatActivity() {
    private var disposable: Disposable? = null
    private lateinit var loadingDialog: DialogManager
    private lateinit var loginDialog: DialogManager
    private var socketThread: Thread = Thread(ClientConnection())

    private var autoLogin = false

    private var connectionErrorCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

      //  println("LEFUTOTTT: ONCREATE " + ClientConnection.ClientData.loggedIn)


        this.loadingDialog =
            DialogManager(this, "progress", "Connecting to server...", "Loading...")
        this.loginDialog =
            DialogManager(this, "progress", "Please wait for login...", "Loading...")



        if (! ClientConnection.ClientData.loggedIn) {

            connectToServer()

            needHelpTextView.setOnClickListener {
                Toast.makeText(
                    applicationContext,
                    "Clicked Need Help?",
                    Toast.LENGTH_SHORT
                ).show()
            }
            loginButton.setOnClickListener {
                this.loginDialog.show()
                this.handleLogin()
            }

            val sharedPreferences = getSharedPreferences("data-login", Context.MODE_PRIVATE)
            if (sharedPreferences.getString("username", null) != null) {
                editText_username.setText(sharedPreferences.getString("username", ""))
                editText_password.setText(sharedPreferences.getString("password", ""))
                this.autoLogin = true
            }

            listen()

            checkServer()
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        checkServer()
    }

    private fun checkServer() {
        if (! this.loadingDialog.dialog.isShowing && ! ClientConnection.ClientData.connectedToServer) {
            finish()
        }
    }

    private fun handleLogin() {
        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "loginConnect",
                hashMapOf(
                    "username" to editText_username.text.toString(),
                    "password" to editText_password.text.toString()
                )
            )
        )

        // Wait for answer (1)
        run {
            RxBus.listen(RxEvent.EventOnMessageFromServer::class.java).take(1).filter {
                it.msg.type == "status"
            }.subscribe({
                if ((it.msg as StatusMessage).status) {
                    Log.log(this).info("Correct login data, wait for login token...")

                    RxBus.listen(RxEvent.EventOnMessageFromServer::class.java).take(1).filter { e ->
                        e.msg.type == "status" && (e.msg as StatusMessage).status
                    }.subscribe({ e ->
                        val message = (e.msg as StatusMessage)
                        val loginToken = message.message

                        ClientConnection.ClientData.loginToken = loginToken
                        ClientConnection.ClientData.username = editText_username.text.toString()
                        ClientConnection.ClientData.password = editText_password.text.toString()

                        Log.log(this)
                            .info("The client token arrived from the server, and sending back to the server: $loginToken")

                        this.loginSuccessful()
                        this.sendLoginTokenToServer(loginToken)
                    }, {})

                } else {
                    this.loginUnsuccessful()
                }
            }, {})
        }
    }

    private fun sendLoginTokenToServer(loginToken: String) {
        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "login",
                hashMapOf(
                    "token" to loginToken
                )
            )
        )
    }

    override fun onResume() {
        super.onResume()

        println("LEFUTOTTT: ONRESUME")

        if (ClientConnection.ClientData.loggedIn && ! this.loadingDialog.dialog.isShowing && ! this.loginDialog.dialog.isShowing) {
            this.startBaseActivity()
        }
    }

    private fun loginSuccessful() {
        Log.log(this).info("Successful login! Closing loading dialog.")

        runOnUiThread {
            this.loginDialog.close()
            Toaster.toast(applicationContext, "Login successful!")

            val sharedPreference = getSharedPreferences("data-login", Context.MODE_PRIVATE)
            val editor = sharedPreference.edit()
            editor.putString("username", ClientConnection.ClientData.username)
            editor.putString("password", ClientConnection.ClientData.password)
            editor.apply()

            ClientConnection.ClientData.loggedIn = true

            this.startBaseActivity()
        }
    }

    private fun startBaseActivity() {
        val intent = Intent(this, BaseActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun loginUnsuccessful() {
        Log.log(this).info("Unsuccessful login! Closing loading dialog.")

        runOnUiThread {
            this.loginDialog.close()
            Toaster.toast(applicationContext, "Login failed!")
            DialogManager(this, "error","Incorrect login data", "Please try again...").show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (disposable != null && ! disposable?.isDisposed!!) {
            disposable?.dispose()
        }
        //stopService(this.bluetoothServiceIntent)
    }

    private fun listen() {
        this.disposable = RxBus.listen(RxEvent.EventOnConnectionSocket::class.java).subscribe( {
            when (it.type) {
                "connectionError" -> {
                    connectionErrorCount++
                    if (connectionErrorCount < 10) {
                        this.connectToServer()
                    } else {
                        runOnUiThread {
                            loginDialog.close()
                            loadingDialog.close()

                            val errorActivity = Intent(this, ErrorActivityNoConnection::class.java)
                            errorActivity.putExtra("title", "Server connection error...")
                            errorActivity.putExtra("description", "You have been disconnected. Please try again later.")
                            errorActivity.putExtra("button_cancel", "Exit application")

                            startActivity(errorActivity)

                            finish()
                        }
                    }
                }
                "connectionSuccess" -> this.successfulConnection()
            }
        }, {})
    }

    private fun successfulConnection() {
        this.loadingDialog.close()
        if (this.autoLogin) {
            handleLogin()
        }
    }

    private fun connectToServer() {
        runOnUiThread {
            loadingDialog.reopen()
        }

        socketThread.interrupt()
        GlobalScope.launch {
            while (socketThread.state != Thread.State.TERMINATED && socketThread.state != Thread.State.NEW) {
                delay(1000)
            }

            socketThread = Thread(ClientConnection())
            socketThread.start()
        }
    }

}
