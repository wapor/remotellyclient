package com.example.sockettestkotlin.volume

import android.content.Context
import android.database.ContentObserver
import android.media.AudioManager
import android.os.Handler
import android.view.KeyEvent
import com.bus.RxBus.publish
import com.bus.RxEvent.EventOnKeyListener

class SettingsContentObserver(
    var context: Context,
    handler: Handler?
) : ContentObserver(handler) {
    var previousVolume: Int
    override fun deliverSelfNotifications(): Boolean {
        return super.deliverSelfNotifications()
    }

    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
        val audio =
            context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC)
        val delta = previousVolume - currentVolume
        val maxValue = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC)

        if (delta > 0) {
            if (currentVolume != maxValue / 2) {
                publish(EventOnKeyListener(KeyEvent.KEYCODE_VOLUME_DOWN))
            }
            previousVolume = currentVolume
        } else if (delta < 0) {
            if (currentVolume != maxValue / 2) {
                publish(EventOnKeyListener(KeyEvent.KEYCODE_VOLUME_UP))
            }
            previousVolume = currentVolume
        }

        println(maxValue)

        audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxValue/2, 0)
    }

    init {
        val audio =
            context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC)
    }
}