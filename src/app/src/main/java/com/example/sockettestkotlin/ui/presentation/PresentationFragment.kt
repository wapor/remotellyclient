package com.example.sockettestkotlin.ui.presentation

import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bus.RxBus
import com.bus.RxEvent
import com.example.sockettestkotlin.DialogManager
import com.example.sockettestkotlin.R
import com.github.chrisbanes.photoview.PhotoView
import com.topin.services.ClientConnection
import com.topin.services.Log
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_presentation.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class PresentationFragment : Fragment() {

    private var mediaPlayer: MediaPlayer? = null
    private var loadingDialog: DialogManager? = null
    private lateinit var updateViewListener: Disposable
    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_presentation, container, false)


        listening()

        run {
            updateViewListener = RxBus.listen(RxEvent.EventOnUpdateView::class.java).subscribe( {
                when (it.type) {
                    "screenshotFragment" -> {
                        activity?.runOnUiThread {
                            val scr = root.findViewById<PhotoView>(R.id.image_screenshot_presentation)
                            val strBase64 = it.data["imageBase64"].toString()


                            val decodedString: ByteArray = android.util.Base64.decode(strBase64, android.util.Base64.DEFAULT)
                            val decodedByte =
                                BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

                            scr.setImageBitmap(decodedByte)
                        }
                    }
                }
            }, {
                it.printStackTrace()
            })
        }


        root.button_presentation_start.setOnClickListener {
            ClientConnection.ClientData.onPresentation = true

            mediaPlayer = MediaPlayer.create(root.context, R.raw.blank)
            if (mediaPlayer != null) {
                mediaPlayer!!.isLooping = true
                mediaPlayer!!.start()
            }

            this.disposable = RxBus.listen(RxEvent.EventOnKeyListener::class.java).takeWhile { ClientConnection.ClientData.onPresentation }.subscribe({
                GlobalScope.launch {
                    delay(850)
                    RxBus.publish(
                        RxEvent.EventCommanderSendCommand(
                            "request",
                            hashMapOf(
                                "request" to "screenshot",
                                "parameter" to "",
                                "from" to ClientConnection.ClientData.loginToken.toString(),
                                "target" to "1"
                            )
                        )
                    )
                }

                if (it.keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                    sendButton(0x27)
                }
                else if (it.keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    sendButton(0x25)
                }
            }, {
                Log.log(this).error(it.message)
            })
        }

        root.button_presentation_stop.setOnClickListener {
            ClientConnection.ClientData.onPresentation = false
            if (disposable != null) {
                this.disposable!!.dispose()
            }

            if (mediaPlayer != null) {
                mediaPlayer!!.stop()
            }
        }

        return root
    }

    private fun listening() {
        this.loadingDialog?.show()
    }


    override fun onDestroy() {
        super.onDestroy()

        updateViewListener.dispose()

        println("onDestroy")

        if (disposable != null) {
            this.disposable!!.dispose()
        }

    }

    fun sendButton(keyCode: Int) {
        RxBus.publish(
            RxEvent.EventCommanderSendCommand(
                "keyCode",
                hashMapOf(
                    "keyCode" to keyCode.toString(),
                    "from" to ClientConnection.ClientData.loginToken.toString(),
                    "target" to "1"
                )
            )
        )
    }
}