package com.example.sockettestkotlin.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.sockettestkotlin.R
import com.example.sockettestkotlin.objects.SingleTaskObject
import kotlinx.android.synthetic.main.fragment_task_list_element.view.*
import kotlinx.android.synthetic.main.list_item.view.*

class TaskListAdapter(private val itemList: ArrayList<SingleTaskObject>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var itemClickListener: OnItemClickListener


    interface OnItemClickListener {
        fun onItemClick(view: View, obj: SingleTaskObject, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.itemClickListener = mItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_task_list_element, parent, false)
        return PlaceViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (viewHolder is TaskListAdapter.PlaceViewHolder) {

            val item = itemList[position]

            viewHolder.titleTextView.text = item.name
            viewHolder.pidTextView.text = item.pid

            //val context = viewHolder.courseHolderView.context


            viewHolder.killButton.setOnClickListener { v: View -> itemClickListener.onItemClick(v, itemList[position], position) }


        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class PlaceViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        internal var titleTextView: TextView = view.task_list_element_title
        internal var pidTextView: TextView = view.task_list_element_pid
        internal var killButton: Button = view.task_list_element_kill_btn

    }
}
