package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class RequestMessage(
    private val request: String, // json
    private val parameter: String
) :
    Message("request") {
    private val status = false

    override fun toJson(): String {
        return JSONObject()
            .put("type", "request")
            .put("request", request)
            .put("parameter", parameter)
            .put("from", this.fromToken)
            .put("target", this.targetToken)
            .toString()
    }

}