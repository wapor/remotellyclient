package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class NoTargetServerMessage : Message("noTargetServer") {
    override fun toJson(): String {
        return JSONObject()
            .put("type", "noTargetServer")
            .toString()
    }
}