package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class LoginConnectMessage(val username: String, val password: String) :
    Message("loginConnect") {
    override fun toJson(): String {
        return JSONObject()
            .put("type", "loginConnect")
            .put("username", username)
            .put("password", password)
            .toString()
    }

}