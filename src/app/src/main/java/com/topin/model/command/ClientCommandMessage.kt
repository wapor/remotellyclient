package com.topin.model.command

import org.json.JSONObject

import com.topin.model.Message

class ClientCommandMessage(private val commandType: String, private val command: String) : Message("command") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "clientCommand")
            .put("commandType", this.commandType)
            .put("command", this.command)
            .put("from", this.fromToken)
            .put("target", this.targetToken)
            .toString()
    }
}
