package com.topin.model.command

import com.topin.model.Message
import com.topin.model.command.helper.SingleTask
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.round


class InitMessage(
    val hostname: String,
    val cpuName: String,
    val localIp: String,
    val osName: String,
    val osVersion: String,
    val biosVersion: String,
    val cpuUsage: Double,
    val ramMax: String,
    val ramUsage: String,
    // Json: C: 11GB/256GB  USED/ALL
    val driveUsage: String,
    // JSON: {count: 121, [{name: 'Total Commander', status: 'idle', 'pid': 12331}]}
    val taskList: String,
    val backgroundImage: String
) : Message("init") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "init")
            .put("hostname", hostname)
            .put("cpuName", cpuName)
            .put("localIp", localIp)
            .put("osName", osName)
            .put("osVersion", osVersion)
            .put("biosVersion", biosVersion)
            .put("cpuUsage", cpuUsage)
            .put("ramMax", ramMax)
            .put("ramUsage", ramUsage)
            .put("driveUsage", driveUsage)
            .put("taskList", taskList)
            .put("backgroundImage", backgroundImage)
            .toString()
    }

    fun getRamPercentage() : Double {
        return roundToDecimals((this.ramUsage.toDouble() / this.ramMax.toDouble() * 100), 1)
    }

    fun getRamUsageText() : String {
        return "${this.ramUsage.toLong()/1024/1024} MB / ${this.ramMax.toLong()/1024/1024} MB"
    }

    fun getTaskListAsArray() : ArrayList<SingleTask> {
        val taskListArrayList: ArrayList<SingleTask> = ArrayList()

        val jsonArray = JSONArray(this.taskList)
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            taskListArrayList.add(SingleTask(jsonObject["pid"] as String, jsonObject["name"] as String, jsonObject["memUsage"] as String))
        }
        return taskListArrayList
    }

    fun roundToDecimals(number: Double, numDecimalPlaces: Int): Double {
        val factor = Math.pow(10.0, numDecimalPlaces.toDouble())
        return Math.round(number * factor) / factor
    }

}