package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class MouseClickMessage(private val button: String, private val mouseType: Int) : Message("mouseClick") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "mouseClick")
            .put("mouseType", mouseType)
            .put("button", button)
            .put("from", fromToken)
            .put("target", targetToken)
            .toString()
    }
}