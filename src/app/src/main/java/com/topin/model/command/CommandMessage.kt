package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class CommandMessage(private val command: String) : Message("command") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "command")
            .put("command", this.command)
            .put("from", this.fromToken)
            .put("target", this.targetToken)
            .toString()
    }
}
