package com.topin.model.builder

import java.util.HashMap

abstract class BuilderBase(protected var type: String) {
    protected var data = HashMap<String, Any>()

    fun add(key: String, value: Any): BuilderBase {
        data[key] = value
        return this
    }

    fun setData(data: Any) {
        this.data = data as HashMap<String, Any>
    }
}
