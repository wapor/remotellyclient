package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class MouseMoveMessage(private val x: Int, private val y: Int) : Message("mouseMove") {
    override fun toJson(): String {
        return JSONObject()
            .put("type", "mouseMove")
            .put("x", x)
            .put("y", y)
            .put("from", this.fromToken)
            .put("target", this.targetToken)
            .toString()
    }

}