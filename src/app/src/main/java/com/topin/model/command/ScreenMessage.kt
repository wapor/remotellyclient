package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class ScreenMessage(val imageBase64: String) :
    Message("screenshot") {
    override fun toJson(): String {
        return JSONObject()
            .put("type", "screenshot")
            .put("imageBase64", imageBase64)
            .toString()
    }

}