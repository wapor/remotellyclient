package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class KeyCodeMessage(private val keyCode: Int) : Message("keyCode") {
    override fun toJson(): String {
        return JSONObject()
            .put("type", "keyCode")
            .put("keyCode", keyCode)
            .put("from", fromToken)
            .put("target", targetToken)
            .toString()
    }

}