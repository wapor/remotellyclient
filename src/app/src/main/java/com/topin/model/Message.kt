package com.topin.model

import com.topin.model.contracts.MessageContract

abstract class Message(var type: String?) : MessageContract {
    var fromToken: String? = null
    var targetToken: String? = null

    override fun toString(): String {
        return toJson().toString()
    }
}
