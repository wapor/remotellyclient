package com.topin.model.command

import org.json.JSONObject
import com.topin.model.Message

class StatusMessage(val status: Boolean, val message: String) : Message("status") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "status")
            .put("success", this.status)
            .put("message", this.message)
            .toString()
    }
}
