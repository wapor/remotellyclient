package com.topin.model.builder

import com.topin.helpers.JsonHelper
import com.topin.model.Message
import com.topin.model.command.*
import com.topin.model.contracts.MessageContract
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class MessageBuilder(type: String) : BuilderBase(type) {


    fun get(): MessageContract {
        val messageBuilder: MessageContract
        try {
            when (this.type) {
                "status" -> messageBuilder = StatusMessage((this.data["success"] as Boolean?)!!, (this.data["message"] as String?)!!)
                "command" -> messageBuilder = CommandMessage((this.data["command"] as String?)!!)
                "noTargetServer" -> messageBuilder = NoTargetServerMessage()
                "init" -> {
                    var cpuUsage = data["cpuUsage"].toString() + ""
                    if (!cpuUsage.contains(".")) {
                        cpuUsage += ".0"
                    }
                    val cpuUsageDouble = java.lang.Double.valueOf(cpuUsage)
                    messageBuilder = InitMessage(
                        (data["hostname"] as String?)!!,
                        (data["cpuName"] as String?)!!,
                        (data["localIp"] as String?)!!,
                        (data["osName"] as String?)!!,
                        (data["osVersion"] as String?)!!,
                        (data["biosVersion"] as String?)!!,
                        cpuUsageDouble,
                        (data["ramMax"] as String?)!!,
                        (data["ramUsage"] as String?)!!,
                        (data["driveUsage"] as String?)!!,
                        (data["taskList"] as String?)!!,
                        (data["backgroundImage"] as String?)!!
                    )
                }
                "screenshot" -> messageBuilder = ScreenMessage((this.data["imageBase64"] as String?)!!)
                else -> messageBuilder = UndefinedMessage("undefined")
            }
            return messageBuilder
        } catch (e: NullPointerException) {
            println("Error in json string (not syntax error). Called undefined parameter inside json object.")
            e.printStackTrace()
        }
        return UndefinedMessage("undefined")
    }

    companion object {

        fun build(jsonData: String): Message? {
            try {
                val jsonObject = JSONObject(jsonData)
                if (!jsonObject.has("type")) {
                    println("The JSON do not have 'type' parameter: $jsonData")
                }
                val messageBuilder = MessageBuilder(jsonObject.get("type") as String)
                messageBuilder.data = JsonHelper.toMap(jsonObject) as HashMap<String, Any>
                return messageBuilder.get() as Message
            } catch (err: JSONException) {
                println("Json exception on message: $jsonData")
            }

            return null
        }
    }


}
