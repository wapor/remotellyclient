package com.topin.model.command

import com.topin.model.Message
import org.json.JSONObject

class LoginMessage(val clientType: String, val token: String) : Message("login") {

    override fun toJson(): String {
        return JSONObject()
            .put("type", "login")
            .put("clientType", this.clientType)
            .put("token", this.token)
            .toString()
    }
}
