package com.topin.model.contracts

interface MessageContract {
    fun toJson(): String
}
