package com.topin.model.command


import com.topin.model.Message

class UndefinedMessage(type: String) : Message(type) {

    override fun toJson(): String {
        return ""
    }
}
