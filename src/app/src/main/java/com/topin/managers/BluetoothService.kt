package com.topin.managers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.bluetooth.DeviceManager
import com.example.sockettestkotlin.MainActivity
import com.example.sockettestkotlin.R


class BluetoothService : Service()
{
    private lateinit var deviceManager: DeviceManager
    private val CHANNEL_ID = "Remotelly"

    companion object {
        var isActive = false

        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, BluetoothService::class.java)
           // startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)

            isActive = true
        }
        fun stopService(context: Context) {
            val stopIntent = Intent(context, BluetoothService::class.java)
            context.stopService(stopIntent)

            isActive = false
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Remotelly Listener")
            .setContentText(input)
            .setSmallIcon(R.drawable.logo)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)

        val filter = IntentFilter()
        filter.priority = IntentFilter.SYSTEM_HIGH_PRIORITY
        filter.addAction(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)

        this.deviceManager = DeviceManager(this)

        return START_NOT_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Remotelly Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        this.deviceManager.stopReceiver()
    }

    /*override fun onHandleIntent(intent: Intent?) {
      //  this.deviceManager = DeviceManager(this)
       // this.deviceManager.start()

        GlobalScope.launch {
            while(true) {
                val bluetoothDeviceRssi= deviceManager.getPairedDeviceList().getByName("LAPTOP-N8IQGLA0")
                if (bluetoothDeviceRssi !== null && bluetoothDeviceRssi.rssi != null) {
                    handleRssi(bluetoothDeviceRssi)
                }
                println(deviceManager.getPairedDeviceList().getByName("LAPTOP-N8IQGLA0"))
                delay(500)
            }
        }
    }

    private fun handleRssi(bluetoothDeviceRssi: BluetoothDeviceRssi?) {
        if (bluetoothDeviceRssi!!.rssi!! < -70) {
            RxBus.publish(
                RxEvent.EventCommanderSendCommand(
                    "lock",
                    hashMapOf("target" to "12345", "from" to "0123")
                )
            )
        }
    }*/

}