package com.topin.services

import android.content.Intent
import android.os.Bundle
import com.bus.RxBus
import com.bus.RxEvent
import com.topin.model.Message
import com.topin.model.builder.MessageBuilder
import com.topin.model.command.InitMessage
import com.topin.model.command.NoTargetServerMessage
import com.topin.model.command.ScreenMessage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader


open class MessageReceiver() : Runnable {

    override fun run() {
        ClientConnection.ClientData.socket.use {

            var responseString : String
            val bufferReader = BufferedReader(InputStreamReader(it.inputStream))
            while (true) {
                try {
                    responseString = bufferReader.readLine()

                    val buildMessage = MessageBuilder.build(responseString)
                    if (buildMessage != null) {
                        this.onMessage(buildMessage)
                    }

                } catch (e: Exception) {
                    Log.log(this).error("Socket connection error, while waiting data ${e.message}")
                    RxBus.publish(RxEvent.EventOnConnectionSocket("connectionError"))
                    bufferReader.close()
                    it.close()

                    break
                }
            }
            //bufferReader.close()
            //it.close()
        }
    }

    private fun onMessage(message : Message) {
        println("Received Json: $message")
        RxBus.publish(RxEvent.EventOnMessageFromServer(message))

        if (message is InitMessage) {
            ClientConnection.ClientData.targetInitData = message
        } else if (message is NoTargetServerMessage) {
            GlobalScope.launch {
                //delay(1000) // Wait for loading BaseActivity
                RxBus.publish(RxEvent.EventNoTargetServerSpecifiedOnCenter(message))
                RxBus.publish(RxEvent.EventOnUpdateView("homeFragment", HashMap()))
            }
        } else if(message is ScreenMessage) { //screenshotFragment
            RxBus.publish(RxEvent.EventOnUpdateView("screenshotFragment", hashMapOf("imageBase64" to message.imageBase64)))
            RxBus.publish(RxEvent.EventOnUpdateView("screenshotFragment", hashMapOf("imageBase64" to message.imageBase64)))
        }

        if(message is InitMessage) {
            RxBus.publish(RxEvent.EventOnUpdateView("homeFragment", HashMap()))
        }
    }
}