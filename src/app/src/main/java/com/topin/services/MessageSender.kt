package com.topin.services


import com.bus.RxBus
import com.bus.RxEvent
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Runnable
import java.net.Socket



open class MessageSender {
    lateinit var bus: Disposable

    fun handle() {
        Log.log(this).info("Message Sender has been started")

        run{
            bus = RxBus.listen(RxEvent.EventOnMessageOnMessageSender::class.java)
                .doOnDispose {
                    Log.log(this).warn("Message Sender: DISPOSED")
                }
                .observeOn(Schedulers.io()).subscribe( {
                    //this.onMessage(it.json.toJson())
                    val message = it.json.toJson()
                    val to = if (ClientConnection.ClientData.loginToken != null)
                        ClientConnection.ClientData.getIdentifier()
                    else
                        "Not Logined user (${ClientConnection.ClientData.socket.inetAddress})"

                    Log.log(this).info("Message Sender to: $to : $message ")
                    ClientConnection.ClientData.socket.getOutputStream()
                        .write((message + "\n").toByteArray())
            }, {
                    it.printStackTrace()
                    RxBus.publish(RxEvent.EventOnConnectionSocket("connectionError"))
                })
        }
    }
}