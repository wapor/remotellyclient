package com.topin.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory

object Log {
    private var loggerList = HashMap<String, Logger>()

    fun log(type: Any) : Logger {
        return log(type.javaClass.toString())
    }

    fun log(type: String) : Logger {
        if (loggerList[type] == null) {
            loggerList[type] = LoggerFactory.getLogger(type)
        }
        return loggerList[type]!!
    }
}