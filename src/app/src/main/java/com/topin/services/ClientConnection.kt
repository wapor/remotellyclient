package com.topin.services

import com.bus.RxBus
import com.bus.RxEvent
import com.topin.model.command.InitMessage
import com.topin.model.command.RequestMessage
import com.topin.services.events.CommandListener
import kotlinx.android.synthetic.main.activity_login.*
import java.net.InetSocketAddress
import java.net.Socket


class ClientConnection : Runnable {

    object ClientData {
        var loginToken: String? = null
        var username: String? = null
        var password: String? = null
        var connectedToServer: Boolean = false
        var socket: Socket = Socket()
        var targetInitData: InitMessage? = null

        fun getIdentifier() = "$username-$loginToken"

        var loggedIn = false

        var screenOn: Boolean = true
        var onPresentation: Boolean = false
    }

    object EventControllers {
        var init = false
        var sender: MessageSender = MessageSender() // Listen message sender
        var commands: CommandListener = CommandListener() // Listen commands
       // var receiver: MessageReceiver = MessageReceiver()

        fun start() {
            if (! this.init) {
                sender.handle()
                commands.handle()
                this.init = true
            }
        }
    }


    override fun run() {
        Log.log(this).debug("Start thread ClientConnection")
        EventControllers.start()

        try {
            Log.log(this).info("Trying to connect to server...")
            //Thread.sleep(3000) // TODO: Hogy megjelenjen a loading felirat !!!!! EZ KELLL!!!!!!!!!

            ClientData.socket = Socket()
            ClientData.socket.connect(InetSocketAddress("147.135.149.163", 7777), 5000)
            RxBus.publish(RxEvent.EventOnConnectionSocket("connectionSuccess"))

            Log.log(this).info("Successfully connected to server.")

            ClientData.connectedToServer = true

            // Start Message Receiver event listener
            Thread(MessageReceiver()).start()

            // Start Message Sender event listener
            //EventControllers.sender.handle()

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.log(this).error("Socket error: $e")
            handleErrors()
        }
    }

    private fun handleErrors() {
        RxBus.publish(RxEvent.EventOnConnectionSocket("connectionError"))
    }





}