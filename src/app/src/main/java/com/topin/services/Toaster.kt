package com.topin.services

import android.content.Context
import android.widget.Toast

object Toaster {
    fun toast(applicationContext: Context, msg: String) {
        Toast.makeText(
            applicationContext,
            msg,
            Toast.LENGTH_LONG
        ).show()
    }
}