package com.topin.services.events

import com.bus.RxBus
import com.bus.RxEvent
import com.topin.model.Message
import com.topin.model.command.*
import com.topin.services.Log

class CommandListener {
    fun handle() {
        run{
            RxBus.listen(RxEvent.EventCommanderSendCommand::class.java).subscribe( {
                val msgObject = when(it.type) {
                    "login" -> LoginMessage("client", it.dataSet?.get("token")!!)
                    "loginConnect" -> LoginConnectMessage( it.dataSet?.get("username")!!, it.dataSet["password"]!!)
                    "lock" -> ClientCommandMessage("lock", "")
                    "restart" -> ClientCommandMessage("restart", "")
                    "shutdown" -> ClientCommandMessage("shutdown", "")
                    "shutdownCancel" -> ClientCommandMessage("shutdownCancel", "")
                    "alert" -> ClientCommandMessage("alert", it.dataSet?.get("command")!!)
                    "customCommand" -> setupCustomCommandMessageObject(it.dataSet?.get("command"))
                    "volumeControlMute" -> ClientCommandMessage("volumeControlMute", "")
                    "volumeControlDown" -> ClientCommandMessage("volumeControlDown", "")
                    "volumeControlUp" -> ClientCommandMessage("volumeControlUp", "")
                    "playSound" -> ClientCommandMessage("playSound", it.dataSet?.get("command")!!)
                    "request" -> RequestMessage(it.dataSet?.get("request")!!, it.dataSet?.get("parameter")!!)
                    "taskKill" -> ClientCommandMessage("taskKill", it.dataSet?.get("command")!!)
                    "mouseMove" -> MouseMoveMessage(it.dataSet?.get("x")!!.toInt(), it.dataSet?.get("y")!!.toInt())
                    "mouseClick" -> MouseClickMessage(it.dataSet?.get("button")!!, it.dataSet?.get("mouseType")!!.toInt())
                    "openBrowser" -> ClientCommandMessage("openBrowser", "")
                    "startProcess" -> ClientCommandMessage("startProcess", it.dataSet?.get("command")!!.toString())
                    "keyCode" -> KeyCodeMessage(it.dataSet?.get("keyCode")!!.toInt())
                    else -> UndefinedMessage(it.type)
                }

                if (it.dataSet?.get("target") != null) {
                    msgObject.targetToken = it.dataSet["target"]
                }
                if (it.dataSet?.get("from") != null) {
                    msgObject.fromToken = it.dataSet["from"]
                }

                if (msgObject is UndefinedMessage) {
                    Log.log(this).error("MessageSender get an undefined type: ${it.type}")
                    Log.log(this).error("Please create new type in MessageSender switch-case: ${it.type}")
                }

                RxBus.publish(RxEvent.EventOnMessageOnMessageSender(msgObject))
            }, {
                Log.log(this).error(it.message)
            })
        }
    }

    private fun setupCustomCommandMessageObject(command: String?): Message {
        if (command == null) {
           return UndefinedMessage("customCommand")
        }
        val msg = ClientCommandMessage("customCommand", command)
        Log.log(this).info("Setup custom command action: $command")
        return msg
    }
}